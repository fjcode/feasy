package cn.com.flever.component;

import cn.com.flever.utils.ApiUploadUtils;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;

public class ApiMdUpload extends JDialog {

    private Project project;//当前idea的project
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextField textField2;
    private JButton selectButton;
    private VirtualFile currentFile;//当前选择的文件目录
    private String path = "";//代码生成路径
    private String packagePath = "";//生成代码在project中的包路径


    public ApiMdUpload(Project project, VirtualFile generateFolder) {
        //init
        this.project = project;
        String generatePath=null;
        if(generateFolder!=null&&generateFolder.isDirectory())
        {
            currentFile=generateFolder;
            generatePath=currentFile.getPath();
        }
        if (!StringUtils.isEmpty(generatePath)) {
            //目录不为空则记录入输入框
            path = generatePath;
            //目录格式化
            packagePath = path.replace(project.getBasePath() + "/src/main/java/", "");
            packagePath = packagePath.replace("/", ".");
        }
        //设置界面
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Generator Code");
        textField2.setText(path);

        //generate按钮生成代码监听
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        //生成目录选择监听
        selectButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //初始化fileChoose，并获得选择的目录
                String tmpPath = selectPath(project, true, false,currentFile);
                textField2.setText(tmpPath);
                path = tmpPath;
                //参数格式化
                packagePath = tmpPath.replace(project.getBasePath() + "/src/main/java/", "");
                packagePath = packagePath.replace("/", ".");
            }
        });

        // X关闭按钮事件
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // 键盘Esc关闭事件
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }


    /**
     * 唤出fileChoose
     *
     * @param project       当前所在项目
     * @param chooseFile    是否选择文件
     * @param chooseFolder  是否选择目录
     * @param currentFolder 当前鼠标选择的目录
     * @return
     */
    public static String selectPath(Project project, boolean chooseFile, boolean chooseFolder, VirtualFile currentFolder) {
        //生成fileChoose
        FileChooserDescriptor fileChooserDescriptor = new FileChooserDescriptor(chooseFile, chooseFolder, false, false, false, false);
        //设置当前目录
        VirtualFile selectFile = project.getBaseDir();
        //获取选择的文件
        VirtualFile file = FileChooser.chooseFile(fileChooserDescriptor, project, currentFolder == null ? selectFile : currentFolder);
        return file.getPath();
    }

    private void onOK() {
        ApiUploadUtils.upload(new File(path));
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setCurrentFile(VirtualFile currentFile) {
        this.currentFile = currentFile;
    }
}
