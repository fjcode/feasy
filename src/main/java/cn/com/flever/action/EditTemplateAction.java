package cn.com.flever.action;


import cn.com.flever.component.ApiMdUpload;
import cn.com.flever.component.GenerateCodeDialog;
import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.psi.PsiDirectory;
import org.jetbrains.annotations.NotNull;
import com.intellij.ide.util.*;

import java.io.File;

public class EditTemplateAction extends AnAction {

    public static ApiMdUpload dialog;


    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        //获取当前idea的project
        Project project = e.getProject();
        //获取当前idea中鼠标选择的目录
        IdeView ideView = e.getRequiredData(LangDataKeys.IDE_VIEW);
        PsiDirectory directory = ideView.getOrChooseDirectory();
        VirtualFile selectDirectory =null;
        if(directory!=null)
        {
            selectDirectory= directory.getVirtualFile();
        }
        if (dialog == null) {
            //生成代码生成器选择目录界面
            dialog = new ApiMdUpload(project, selectDirectory);
        }
        else
        {
            //设置当前目录
            dialog.setCurrentFile(selectDirectory);
            if(selectDirectory!=null&&selectDirectory.isDirectory())
            {
                //目录不为空则记录入输入框
                String path = selectDirectory.getPath();
                //目录格式化
                String packagePath = path.replace(project.getBasePath() + "/src/main/java/", "");
                packagePath = packagePath.replace("/", ".");
                dialog.setPath(path);
            }

        }
        //显示代码生成器界面
        dialog.pack();
        dialog.setLocationRelativeTo(WindowManager.getInstance().getFrame(project));
        dialog.setVisible(true);
    }






}
