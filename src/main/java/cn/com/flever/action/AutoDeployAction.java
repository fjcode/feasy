package cn.com.flever.action;

import cn.com.flever.component.AutoDeployDialog;
import cn.com.flever.component.GenerateUnitCode;
import cn.com.flever.utils.ConsoleUtils;
import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.WindowManager;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

/**
 * 自动部署测试环境动作，快捷键ALT+L
 */
public class AutoDeployAction extends AnAction {

    public static AutoDeployDialog dialog;

    public static String cookieFilePath="D://sym3Cookie.ini";


    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        //获取当前idea的project
        Project project = e.getProject();
        //获取当前idea中鼠标选择的目录
        //生成代码生成器选择目录界面
        dialog = new AutoDeployDialog(project);
        dialog.getTextField1().setText(AutoDeployDialog.tagMap.containsKey(project.getName())?AutoDeployDialog.tagMap.get(project.getName()):"v-sym3-0.0.1-alpha.001");
        try {
            File cookieFile=new File(cookieFilePath);
            if(!cookieFile.exists())
            {
                cookieFile.createNewFile();
            }else{
                String cookie = FileUtils.readFileToString(cookieFile, "UTF-8");
                dialog.getA22TextField().setText(cookie);
            }
        } catch (Exception ioException) {
            ioException.printStackTrace();
        }
        //显示代码生成器界面
        dialog.pack();
        dialog.setLocationRelativeTo(WindowManager.getInstance().getFrame(project));
        dialog.setVisible(true);

    }


}
