package cn.com.flever.action;

import cn.com.flever.component.GenerateCodeDialog;
import cn.com.flever.component.GenerateUnitCode;
import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.psi.PsiDirectory;
import org.jetbrains.annotations.NotNull;

/**
 * 模板生成动作，快捷键ALT+G
 */
public class GenerateUnitCodeAction extends AnAction {

    public static GenerateUnitCode dialog;


    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        //获取当前idea的project
        Project project = e.getProject();
        //获取当前idea中鼠标选择的目录
        if (dialog == null) {
            //生成代码生成器选择目录界面
            dialog = new GenerateUnitCode(project);
        }
        else
        {
            //目录不为空则记录入输入框
            //目录格式化
            String packagePath = project.getBasePath() + "/src/";
            dialog.setPackagePath(packagePath);
        }
        //显示代码生成器界面
        dialog.pack();
        dialog.setLocationRelativeTo(WindowManager.getInstance().getFrame(project));
        dialog.setVisible(true);
    }


}
