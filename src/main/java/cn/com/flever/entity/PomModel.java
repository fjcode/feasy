package cn.com.flever.entity;

/**
 * pom.xml解析model
 */
public class PomModel {

    private String name;

    private String artifactId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }


}
