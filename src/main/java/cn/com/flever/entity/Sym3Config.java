package cn.com.flever.entity;

import cn.com.flever.utils.FTPUtil;
import cn.hutool.extra.ftp.Ftp;
import cn.hutool.http.HttpUtil;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 　* @Description: TODO
 * 　* @author qiyu
 * 　* @date 2021/4/7 16:52
 *
 */
public class Sym3Config {

    public static final String serviceConfig="{\n" +
            "    \"k8sService\":{\n" +
            "        \"sessionAffinity\":\"None\",\n" +
            "        \"selector\":{\n" +
            "            \"project\":\"tc\",\n" +
            "            \"group\":\"#PROJECT_CODE#\",\n" +
            "            \"role\":\"app\"\n" +
            "        },\n" +
            "        \"type\":\"ClusterIP\",\n" +
            "        \"ports\":[\n" +
            "            {\n" +
            "                \"targetPort\":8080,\n" +
            "                \"protocol\":\"TCP\",\n" +
            "                \"name\":\"http\",\n" +
            "                \"port\":80\n" +
            "            }\n" +
            "        ],\n" +
            "        \"metadata\":{\n" +
            "            \"labels\":{\n" +
            "                \"project\":\"tc\",\n" +
            "                \"group\":\"#PROJECT_CODE#\",\n" +
            "                \"role\":\"app\"\n" +
            "            },\n" +
            "            \"annotations\":{\n" +
            "                \"service.beta.kubernetes.io/nks-tenant-name\":\"tc\",\n" +
            "                \"service.beta.kubernetes.io/nks-load-balancer-set-vpcid\":\"c013495d-5699-4f93-acc0-b0a2623ac8ee\"\n" +
            "            }\n" +
            "        }\n" +
            "    },\n" +
            "    \"k8sServiceName\":\"#PROJECT_CODE#-app\"\n" +
            "}";

    public static final String configMapConfig="{\n" +
            "   \"data\":{\n" +
            "        \"fundament-x.properties\":\"###############################\\n# SpringBoot debug switch\\n###############################\\ndebug=true\\n\\n\\n###############################\\n# #PROJECT_CODE#\\n###############################\\nproject.code=#PROJECT_CODE#\\nproject.test=true\\n\\n\\n###############################\\n# Directories\\n###############################\\ndir.log=/home/tc/var/#PROJECT_CODE#/log\\ndir.data=/home/tc/var/#PROJECT_CODE#/data\\ndir.tmp=/home/tc/var/#PROJECT_CODE#/tmp\\n\\n\\n###############################\\n# Cookie\\n###############################\\ncookie.domain=test.tc.netease.com\\n\\n\\n###############################\\n# Wechat login\\n###############################\\nwechat.data-center-realm=http://cmp.test.tc.netease.com\\n\\n\\n###############################\\n# COCO\\n###############################\\ncoco.realm=http://tc.netease.com/coco\\n\\n\\n###############################\\n# OA login\\n###############################\\noa.login.realm=http://#PROJECT_CODE#.test.tc.netease.com/\\noa.login.welcome_url=http://#PROJECT_CODE#.test.tc.netease.com\\n\\n\\n###############################\\n# Drink\\n###############################\\n# drink.debugger.white-list-request-url=http://tc-test01-cmp.i.nease.net/api/test_white_list\\ndrink.debugger.white-list-request-url=http://cmp.test.tc.netease.com/api/test_white_list\\n\\n\\n###############################\\n# CMP information\\n###############################\\n# cmp.realm=http://tc-test01-cmp.i.nease.net\\ncmp.realm=http://cmp.test.tc.netease.com\\n\\n\\n###############################\\n# Redis configurations.\\n###############################\\nredis.database.session=0\\nredis.master.session=tc\\nredis.nodes.session=42.186.112.202:26379, 42.186.112.202:26479, 42.186.112.202:26579\\n\\nredis.database.share=1\\nredis.master.share=tc\\nredis.nodes.share=42.186.112.202:26379, 42.186.112.202:26479, 42.186.112.202:26579\\n\\nredis.database.data=2\\nredis.master.data=tc\\nredis.nodes.data=42.186.112.202:26379, 42.186.112.202:26479, 42.186.112.202:26579\\n\\n\\n###############################\\n# Database\\n###############################\\n#database.host=tc-test-100084-m.hz.dumbo.nie.netease.com\\ndatabase.host=tc-test2-103613-m.hz1.dumbo.nie.netease.com\\ndatabase.port=3306\\ndatabase.name=#PROJECT_CODE#_test\\ndatabase.username=#PROJECT_CODE#\\ndatabase.password=ePeieIIG4p8XYskl\\n\\nspring.datasource.jdbc-url = jdbc:mysql://${database.host}:${database.port}/${database.name}?useUnicode=true&characterEncoding=UTF-8&useSSL=false\\nspring.datasource.username = ${database.username}\\nspring.datasource.password = ${database.password}\\nspring.datasource.driverClassName = com.mysql.jdbc.Driver\\nspring.datasource.type = com.zaxxer.hikari.HikariDataSource\\n\\n\\n\\n\\n\\n###############################\\n# Manage actuator endpoints\\n###############################\\nmanagement.endpoint.shutdown.enabled=true\\nmanagement.endpoints.web.exposure.include=info, shutdown\\n\\n\\nserver.tomcat.accesslog.enabled=true  \\n\\nserver.tomcat.accesslog.suffix=.log\\n\\nserver.tomcat.accesslog.prefix=access_log\\n\\nserver.tomcat.accesslog.file-date-format=.yyyy-MM-dd\\n\\nserver.tomcat.basedir=tomcat\\n\\nserver.tomcat.accesslog.directory=/home/tc/var/tomcat/log/\\n\\nlogging.level.org.apache.tomcat=DEBUG\\n\\nlogging.level.org.apache.catalina=DEBUG\\n\\n\\n\\nreport.email.to.user=wb.mjm@mesg.corp.netease.com\\n\\ndrink.request-parameter-session-id=true\\n \\n\\nsyn.job.ips=10.202.35.110\\n\",\n" +
            "        \"logback-spring.xml\":\"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\"?>\\n<configuration debug=\\\"false\\\" scan=\\\"true\\\" scanPeriod=\\\"30 seconds\\\">\\n    <include resource=\\\"org/springframework/boot/logging/logback/defaults.xml\\\" />\\n    <springProperty scope=\\\"context\\\" name=\\\"LOG_HOME\\\" source=\\\"dir.log\\\"/>\\n\\n    <property name=\\\"CONSOLE_LOG_PATTERN\\\" value=\\\"%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} %clr(${LOG_LEVEL_PATTERN:-%5p}) %clr(${PID:- }){magenta} %clr(---){faint} %clr([%15.15t]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint} %m%n${LOG_EXCEPTION_CONVERSION_WORD:-%wEx}\\\"/>\\n\\n\\n    <appender name=\\\"console\\\" class=\\\"ch.qos.logback.core.ConsoleAppender\\\">\\n        <filter class=\\\"ch.qos.logback.classic.filter.ThresholdFilter\\\">\\n            <level>DEBUG</level>\\n        </filter>\\n        <encoder>\\n            <pattern>${CONSOLE_LOG_PATTERN}</pattern>\\n            <charset>utf8</charset>\\n        </encoder>\\n    </appender>\\n\\n    <appender name=\\\"FILE\\\"  class=\\\"ch.qos.logback.core.FileAppender\\\">\\n        <file>/home/tc/var/#PROJECT_CODE#/log/#PROJECT_CODE#.log</file>\\n        <encoder class=\\\"com.netease.nieweb.framework.drink.logback.DrinkLayoutEncoder\\\">\\n            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%h] [%thread] %-5level %logger{50} - %msg%n</pattern>\\n        </encoder>\\n    </appender>\\n\\n    <appender name=\\\"LOG_FILE\\\" class=\\\"ch.qos.logback.core.rolling.RollingFileAppender\\\">\\n        <filter class=\\\"ch.qos.logback.classic.filter.ThresholdFilter\\\">\\n            <level>info</level>\\n        </filter>\\n        <encoder charset=\\\"UTF-8\\\" class=\\\"net.logstash.logback.encoder.LogstashEncoder\\\">\\n        </encoder>\\n        <rollingPolicy class=\\\"ch.qos.logback.core.rolling.TimeBasedRollingPolicy\\\">\\n            <fileNamePattern>/home/tc/var/#PROJECT_CODE#/log/#PROJECT_CODE#-logstash-%d{yyyy-MM-dd}.log</fileNamePattern>\\n        </rollingPolicy>\\n    </appender>\\n\\n    <appender name=\\\"LOG_OUT\\\" class=\\\"ch.qos.logback.core.rolling.RollingFileAppender\\\">\\n        <encoder>\\n            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%h] [%thread] %-5level %logger{50} - %msg%n</pattern>\\n        </encoder>\\n        <rollingPolicy class=\\\"ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy\\\">\\n            <fileNamePattern>/home/tc/var/#PROJECT_CODE#/log/#PROJECT_CODE#.%d{yyyy-MM-dd}.%i.log</fileNamePattern>\\n            <maxFileSize>30MB</maxFileSize>\\n            <maxHistory>365</maxHistory>\\n        </rollingPolicy>\\n    </appender>\\n\\n\\n    <appender name=\\\"HUMBIRD_EVENT\\\" class=\\\"ch.qos.logback.core.rolling.RollingFileAppender\\\">\\n        <filter class=\\\"ch.qos.logback.classic.filter.ThresholdFilter\\\">\\n            <level>info</level>\\n        </filter>\\n        <encoder charset=\\\"UTF-8\\\" class=\\\"net.logstash.logback.encoder.LogstashEncoder\\\">\\n            <includeMdc>true</includeMdc>\\n        </encoder>\\n        <rollingPolicy class=\\\"ch.qos.logback.core.rolling.TimeBasedRollingPolicy\\\">\\n            <fileNamePattern>/home/tc/var/humbird/log/humbird-%d{yyyy-MM-dd}.log</fileNamePattern>\\n            <MaxHistory>3</MaxHistory>\\n            <CleanHistoryOnStart>true</CleanHistoryOnStart>\\n        </rollingPolicy>\\n    </appender>\\n\\n    <logger name=\\\"com.netease.nieweb.framework.drink.handle.Humbird\\\" level=\\\"info\\\">\\n        <appender-ref ref=\\\"HUMBIRD_EVENT\\\"/>\\n    </logger>\\n\\n\\n\\n\\n    <appender name=\\\"STDOUT\\\" class=\\\"ch.qos.logback.core.ConsoleAppender\\\">\\n        <encoder class=\\\"ch.qos.logback.classic.encoder.PatternLayoutEncoder\\\">\\n            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n</pattern>\\n        </encoder>\\n    </appender>\\n\\n\\n\\n\\n    <logger name=\\\"ErrorReporter\\\" level=\\\"DEBUG\\\" />\\n    <logger name=\\\"com.netease.nieweb\\\" level=\\\"DEBUG\\\" />\\n    <logger name=\\\"com.netease.nieweb.framework.drink\\\"  level=\\\"info\\\" />\\n    <logger name=\\\"com.netease.nieweb.framework.drink.junit\\\"  level=\\\"INFO\\\" />\\n    <logger name=\\\"com.netease.nieweb.framework.drink.schedule.DrinkScheduleRunner\\\"  level=\\\"TRACE\\\" />\\n\\n    <logger name=\\\"org.springframework\\\" level=\\\"info\\\" />\\n    <logger name=\\\"org.springframework.boot.web.embedded.tomcat.TomcatWebServer\\\" level=\\\"INFO\\\" />\\n\\n    <root level=\\\"INFO\\\">\\n        <appender-ref ref=\\\"FILE\\\" />\\n        <appender-ref ref=\\\"LOG_FILE\\\" />\\n        <appender-ref ref=\\\"LOG_OUT\\\"/>\\n        <appender-ref ref=\\\"console\\\"/>\\n\\n    </root>\\n\\n</configuration>\\n\",\n" +
            "        \"application-release.properties\":\"#####################drink#####################\\n# Project\\nproject.code=#PROJECT_CODE#\\nproject.test=true\\n\\n# Directories\\ndir.log=/var/log\\ndir.tmp=/var/tmp\\ndir.data=/var/data\\ndrink.class-exclude-patterns=**.demo.**\\n\\n# Coco\\n#coco.realm=false\\n#tcauth.enable=true\\n\\n\\n# Manage actuator endpoints\\nmanagement.endpoint.shutdown.enabled=true\\nmanagement.endpoints.web.exposure.include=info, shutdown\\n\\n\\n\\ndrink.request-parameter-session-id=true\\nproject.mini-program.enabled=true\\n\"\n" +
            "    },\n" +
            "    \"configmapName\":\"#PROJECT_CODE#\"\n" +
            "}";

    public static final String applicationConfig="{\n" +
            "    \"applicationName\":\"#PROJECT_CODE#\",\n" +
            "    \"application\":{\n" +
            "        \"dnsPolicy\":\"ClusterFirst\",\n" +
            "        \"networking\":{\n" +
            "            \"subnet\":\"7d75786a-8b8a-4100-b3df-5c80f6b4aa97\",\n" +
            "            \"vpc\":\"c013495d-5699-4f93-acc0-b0a2623ac8ee\",\n" +
            "            \"tenant\":\"6c23fa6273824ab1a40c9fddbdb124b5\"\n" +
            "        },\n" +
            "        \"shutdowntime\":30,\n" +
            "        \"dnsConfig\":{\n" +
            "            \"options\":[\n" +
            "                {\n" +
            "                    \"name\":\"ndots\",\n" +
            "                    \"value\":\"2\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        \"replicas\":1,\n" +
            "        \"labels\":{\n" +
            "            \"project\":\"tc\",\n" +
            "            \"role\":\"app\",\n" +
            "            \"group\":\"#PROJECT_CODE#\",\n" +
            "            \"sym_version\":\"1612253196785\"\n" +
            "        },\n" +
            "        \"logDriver\":\"syslog\",\n" +
            "        \"strategy\":{\n" +
            "            \"rollingUpdate\":{\n" +
            "                \"maxSurge\":\"26%\",\n" +
            "                \"maxUnavailable\":\"25%\"\n" +
            "            },\n" +
            "            \"type\":\"RollingUpdate\"\n" +
            "        },\n" +
            "        \"restartPolicy\":\"Always\",\n" +
            "        \"volumes\":[\n" +
            "            {\n" +
            "                \"configMap\":{\n" +
            "                    \"name\":\"#PROJECT_CODE#\",\n" +
            "                    \"defaultMode\":420\n" +
            "                },\n" +
            "                \"name\":\"#PROJECT_CODE#\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"sysctl\":[\n" +
            "            \"net.core.somaxconn=8192\"\n" +
            "        ],\n" +
            "        \"oomKillerDisable\":true,\n" +
            "        \"nodeSelector\":{\n" +
            "            \"project\":\"tc\"\n" +
            "        },\n" +
            "        \"containers\":[\n" +
            "            {\n" +
            "                \"reqMemory\":\"1Gi\",\n" +
            "                \"name\":\"#PROJECT_CODE#-app-0\",\n" +
            "                \"image\":\"ncr.nie.netease.com/tc/#PROJECT_CODE#:#PROJECT_TAG#\",\n" +
            "                \"imagePullPolicy\":\"Always\",\n" +
            "                \"volumeMounts\":[\n" +
            "                    {\n" +
            "                        \"mountPath\":\"/home/gpx/conf/#PROJECT_CODE#/\",\n" +
            "                        \"name\":\"#PROJECT_CODE#\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"reqCPU\":\"500m\",\n" +
            "                \"memory\":\"1Gi\",\n" +
            "                \"cpu\":\"500m\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"metadata\":{\n" +
            "            \"labels\":{\n" +
            "                \"role\":\"app\",\n" +
            "                \"group\":\"#PROJECT_CODE#\"\n" +
            "            },\n" +
            "            \"creationTimestamp\":\"2021-01-11T06:23:57Z\",\n" +
            "            \"annotations\":{\n" +
            "                \"v3alpha.symphony.com/application-disable-auto-create-headless-svc\":\"true\",\n" +
            "                \"deployment.kubernetes.io/revision\":\"29\"\n" +
            "            },\n" +
            "            \"resourceVersion\":\"314285328\"\n" +
            "        }\n" +
            "    }\n" +
            "}";

    public static String getConfig(String configName)
    {
        return  HttpUtil.get("http://***.com/"+configName);
    }

   /* public static void getExeFile(String toDir) throws IOException {
        FTPUtil ftpUtil = new FTPUtil("10.202.35.123",21, "test","qp123@qq.com","");
        ftpUtil.downloadFiles("sym3/exe",toDir+File.separator);
        ftpUtil.closeConnect();
    }*/










}
