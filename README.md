**FJCode**

第一版代码生成器插件，能够生成full_model代码，快捷键ALT+G，支持以下功能：<br>
   - <em>1.支持选择代码生成目录</em>
   - <em>2.能够自动合并messages-entity.properties</em>
   - <em>3.能够自动避免覆盖server文件，GenServer文件除外</em>
   - <em>4.自动读取pom.xml文件，获得project code和project name</em>
   - <em>5.自动带入当前鼠标选择的目录为代码生成目录</em>
   - <em>6.增加驼峰检测，自动将pdm文件中的表字段转为驼峰</em>
   - <em>7.能够自动避免覆盖GenMapper.java文件，能够自动合并GenMapper.xml文件</em>

    第二版代码生成器插件，能够生成在线的接口文档,快捷键alt X，支持以下功能：<br>
   - <em>1.选择md文件，生成在线版html访问链接</em>

    第三版代码生成器插件，能够获取最新的单测基类,快捷键alt M，支持以下功能：<br>
   - <em>1.自动生成单测基类</em>

    第四版代码生成器插件，能够自动部署并且打tag,快捷键alt L，支持以下功能：<br>
   - <em>1.项目自动增加CI/CD，前提需要创建master-gray分支.通过tag按键进行打包，并触发CI/CD，且自动更新sym3版本号</em>
   - <em>2.项目自动部署测试环境，前提需要tag存在。自动生成service、自动生成config、自动生成application</em>
**开发注意**
~~~~
要兼容不同版本的idea，请修改bulid.gradle的intellij.version为对应版本即可
~~~~
